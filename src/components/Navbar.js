import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";
import Dropdown1 from "./Dropdown1";
import Dropdown2 from "./Dropdown2";
import Dropdown3 from "./Dropdown3";
import Dropdown4 from "./Dropdown4";
import Dropdown5 from "./Dropdown5";
import Dropdown6 from "./Dropdown6";
import Dropdown7 from "./Dropdown7";
import Dropdown8 from "./Dropdown8";
import Dropdown9 from "./Dropdown9";
import Dropdown10 from "./Dropdown10";
import Dropdown11 from "./Dropdown11";

function Navbar() {
  const [click, setClick] = useState(false);
  const [dropdown1, setDropdown1] = useState(false);
  const [dropdown2, setDropdown2] = useState(false);
  const [dropdown3, setDropdown3] = useState(false);
  const [dropdown4, setDropdown4] = useState(false);
  const [dropdown5, setDropdown5] = useState(false);
  const [dropdown6, setDropdown6] = useState(false);
  const [dropdown7, setDropdown7] = useState(false);
  const [dropdown8, setDropdown8] = useState(false);
  const [dropdown9, setDropdown9] = useState(false);
  const [dropdown10, setDropdown10] = useState(false);
  const [dropdown11, setDropdown11] = useState(false);

  const handleClick = () => setClick(!click);
  const closeMenu = () => {
    if (window.innerWidth > 960) {
      setClick(false);
    }
  };

  const closeMobileMenu = () => {
    if (window.innerWidth < 960) {
      setClick(false);
    }
  };

  const onClick1 = () => {
    if (window.innerWidth < 960) {
      setDropdown1(!dropdown1);
    }
  };
  const onClick2 = () => {
    if (window.innerWidth < 960) {
      setDropdown2(!dropdown2);
    }
  };
  const onClick3 = () => {
    if (window.innerWidth < 960) {
      setDropdown3(!dropdown3);
    }
  };
  const onClick4 = () => {
    if (window.innerWidth < 960) {
      setDropdown4(!dropdown4);
    }
  };
  const onClick5 = () => {
    if (window.innerWidth < 960) {
      setDropdown5(!dropdown5);
    }
  };
  const onClick6 = () => {
    if (window.innerWidth < 960) {
      setDropdown6(!dropdown6);
    }
  };
  const onClick7 = () => {
    if (window.innerWidth < 960) {
      setDropdown7(!dropdown7);
    }
  };
  const onClick8 = () => {
    if (window.innerWidth < 960) {
      setDropdown8(!dropdown8);
    }
  };
  const onClick9 = () => {
    if (window.innerWidth < 960) {
      setDropdown9(!dropdown9);
    }
  };
  const onClick10 = () => {
    if (window.innerWidth < 960) {
      setDropdown10(!dropdown10);
    }
  };
  const onClick11 = () => {
    if (window.innerWidth < 960) {
      setDropdown11(!dropdown11);
    }
  };
  const onMouseEnter1 = () => {
    if (window.innerWidth < 960) {
      setDropdown1(false);
    } else {
      setDropdown1(true);
    }
  };
  const onMouseLeave1 = () => {
    if (window.innerWidth < 960) {
      setDropdown1(false);
    } else {
      setDropdown1(false);
    }
  };

  const onMouseEnter2 = () => {
    if (window.innerWidth < 960) {
      setDropdown2(false);
    } else {
      setDropdown2(true);
    }
  };
  const onMouseLeave2 = () => {
    if (window.innerWidth < 960) {
      setDropdown2(false);
    } else {
      setDropdown2(false);
    }
  };

  const onMouseEnter3 = () => {
    if (window.innerWidth < 960) {
      setDropdown3(false);
    } else {
      setDropdown3(true);
    }
  };
  const onMouseLeave3 = () => {
    if (window.innerWidth < 960) {
      setDropdown3(false);
    } else {
      setDropdown3(false);
    }
  };

  const onMouseEnter4 = () => {
    if (window.innerWidth < 960) {
      setDropdown4(false);
    } else {
      setDropdown4(true);
    }
  };
  const onMouseLeave4 = () => {
    if (window.innerWidth < 960) {
      setDropdown4(false);
    } else {
      setDropdown4(false);
    }
  };

  const onMouseEnter5 = () => {
    if (window.innerWidth < 960) {
      setDropdown5(false);
    } else {
      setDropdown5(true);
    }
  };
  const onMouseLeave5 = () => {
    if (window.innerWidth < 960) {
      setDropdown5(false);
    } else {
      setDropdown5(false);
    }
  };

  const onMouseEnter6 = () => {
    if (window.innerWidth < 960) {
      setDropdown6(false);
    } else {
      setDropdown6(true);
    }
  };
  const onMouseLeave6 = () => {
    if (window.innerWidth < 960) {
      setDropdown6(false);
    } else {
      setDropdown6(false);
    }
  };

  const onMouseEnter7 = () => {
    if (window.innerWidth < 960) {
      setDropdown7(false);
    } else {
      setDropdown7(true);
    }
  };
  const onMouseLeave7 = () => {
    if (window.innerWidth < 960) {
      setDropdown7(false);
    } else {
      setDropdown7(false);
    }
  };

  const onMouseEnter8 = () => {
    if (window.innerWidth < 960) {
      setDropdown8(false);
    } else {
      setDropdown8(true);
    }
  };
  const onMouseLeave8 = () => {
    if (window.innerWidth < 960) {
      setDropdown8(false);
    } else {
      setDropdown8(false);
    }
  };

  const onMouseEnter9 = () => {
    if (window.innerWidth < 960) {
      setDropdown9(false);
    } else {
      setDropdown9(true);
    }
  };
  const onMouseLeave9 = () => {
    if (window.innerWidth < 960) {
      setDropdown9(false);
    } else {
      setDropdown9(false);
    }
  };

  const onMouseEnter10 = () => {
    if (window.innerWidth < 960) {
      setDropdown10(false);
    } else {
      setDropdown10(true);
    }
  };
  const onMouseLeave10 = () => {
    if (window.innerWidth < 960) {
      setDropdown10(false);
    } else {
      setDropdown10(false);
    }
  };

  const onMouseEnter11 = () => {
    if (window.innerWidth < 960) {
      setDropdown11(false);
    } else {
      setDropdown11(true);
    }
  };
  const onMouseLeave11 = () => {
    if (window.innerWidth < 960) {
      setDropdown11(false);
    } else {
      setDropdown11(false);
    }
  };

  return (
    <>
      <nav className="navbar">
        <Link to="/" className="navbar-logo" onClick={closeMenu}>
          Pharma
          <span>
            <i className="fas fa-user-nurse"></i>
          </span>
        </Link>
        <div className="menu-icon" onClick={handleClick}>
          <i className={click ? "fas fa-times" : "fas fa-bars"} />
        </div>
        <ul className={click ? "nav-menu active" : "nav-menu"}>
          <li
            className="nav-item"
            onMouseEnter={onMouseEnter1}
            onMouseLeave={onMouseLeave1}
            onClick={onClick1}
          >
            <div className="nav-links" onClick={closeMenu}>
              Home{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown1 && <Dropdown1 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter2}
            onMouseLeave={onMouseLeave2}
            onClick={onClick2}
          >
            <div className="nav-links" onClick={closeMenu}>
              Explore Us{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown2 && <Dropdown2 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter3}
            onMouseLeave={onMouseLeave3}
            onClick={onClick3}
          >
            <div className="nav-links" onClick={closeMenu}>
              Admissions{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown3 && <Dropdown3 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter4}
            onMouseLeave={onMouseLeave4}
            onClick={onClick4}
          >
            <div className="nav-links" onClick={closeMenu}>
              Departments{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown4 && <Dropdown4 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter5}
            onMouseLeave={onMouseLeave5}
            onClick={onClick5}
          >
            <div className="nav-links" onClick={closeMenu}>
              Research{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown5 && <Dropdown5 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter6}
            onMouseLeave={onMouseLeave6}
            onClick={onClick6}
          >
            <div className="nav-links" onClick={closeMenu}>
              Academic Excellence{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown6 && <Dropdown6 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter7}
            onMouseLeave={onMouseLeave7}
            onClick={onClick7}
          >
            <div className="nav-links" onClick={closeMenu}>
              Staff{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown7 && <Dropdown7 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter8}
            onMouseLeave={onMouseLeave8}
            onClick={onClick8}
          >
            <div className="nav-links" onClick={closeMenu}>
              Facilities{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown8 && <Dropdown8 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter9}
            onMouseLeave={onMouseLeave9}
            onClick={onClick9}
          >
            <div className="nav-links" onClick={closeMenu}>
              Gallery{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown9 && <Dropdown9 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter10}
            onMouseLeave={onMouseLeave10}
            onClick={onClick10}
          >
            <div className="nav-links" onClick={closeMenu}>
              Alumni{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown10 && <Dropdown10 closeMenu={closeMobileMenu} />}
          </li>

          <li
            className="nav-item"
            onMouseEnter={onMouseEnter11}
            onMouseLeave={onMouseLeave11}
            onClick={onClick11}
          >
            <div className="nav-links" onClick={closeMenu}>
              Contact Us{" "}
              <span>
                <i className="fas fa-angle-right"></i>
              </span>
              <span>
                <i className="fas fa-caret-down" />
              </span>
            </div>

            {dropdown11 && <Dropdown11 closeMenu={closeMobileMenu} />}
          </li>
        </ul>
      </nav>
    </>
  );
}

export default Navbar;
