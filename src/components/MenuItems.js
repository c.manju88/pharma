export const MenuItems1 = [
  {
    title: "Marketing",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];

export const MenuItems2 = [
  {
    title: "Second",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];

export const MenuItems3 = [
  {
    title: "Third",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
export const MenuItems4 = [
  {
    title: "Fourth",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
export const MenuItems5 = [
  {
    title: "Fifth",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
export const MenuItems6 = [
  {
    title: "Sixth",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
export const MenuItems7 = [
  {
    title: "Seventh",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
export const MenuItems8 = [
  {
    title: "Eighth",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
export const MenuItems9 = [
  {
    title: "Ninth",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
export const MenuItems10 = [
  {
    title: "Tenth",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
export const MenuItems11 = [
  {
    title: "Eleventh",
    path: "/marketing",
    cName: "dropdown-link",
  },
  {
    title: "Consulting",
    path: "/consulting",
    cName: "dropdown-link",
  },
  {
    title: "Design",
    path: "/design",
    cName: "dropdown-link",
  },
  {
    title: "Development",
    path: "/development",
    cName: "dropdown-link",
  },
];
