import React, { useState } from "react";
import { MenuItems1 } from "./MenuItems";
import "./Dropdown1.css";
import { Link } from "react-router-dom";

function Dropdown(props) {
  const [click, setClick] = useState(false);
  // const [classes, setClasses] = useState(false);

  const handleClick = () => {
    setClick(!click);
    props.closeMenu();
  };

  // if (window.innerWidth < 960) {
  //   console.log("From Dropdown");
  //   setClasses(!classes);
  // }

  return (
    <>
      <ul
        onClick={handleClick}
        className={click ? "dropdown-menu clicked" : "dropdown-menu"}
      >
        {MenuItems1.map((item, index) => {
          return (
            <li key={index}>
              <Link
                className={item.cName}
                to={item.path}
                onClick={() => setClick(false)}
              >
                {item.title}
              </Link>
            </li>
          );
        })}
      </ul>
    </>
  );
}

export default Dropdown;
